package cz.trigon.ld37.world;

import cz.dat.gaben.util.Vector2;

public class CircleBB implements IBoundingBox<CircleBB> {
    public float radius;
    public Vector2 center;

    public static CircleBB makeCircleBBForIsoscelesTriangle(float base, float height) {
        float b = (float) Math.sqrt((height * height) + ((base * base) / 4));
        float r = (b * b) / (float) Math.sqrt(4 * b * b - base * base);
        return new CircleBB(r, base / 2, height - r);
    }

    public CircleBB(CircleBB original) {
        this(original.radius, original.center.x(), original.center.y());
    }

    public CircleBB() {
        this.radius = 0;
        this.center = new Vector2();
    }

    public CircleBB(float r, Vector2 center) {
        this.radius = r;
        this.center = center;
    }

    public CircleBB(float r, float centerX, float centerY) {
        this.radius = r;
        this.center = new Vector2(centerX, centerY);
    }

    @Override
    public CircleBB mix(CircleBB other, float m) {
        float deltaX0 = other.center.x() - this.center.x();
        float deltaY0 = other.center.y() - this.center.y();
        float deltaR = other.radius - this.radius;

        float x = this.center.x() + deltaX0 * m;
        float y = this.center.y() + deltaY0 * m;
        float r = this.radius + deltaR * m;

        return new CircleBB(r, x, y);
    }

    @Override
    public boolean collidesWith(CircleBB other, float vX, float vY) {
        float r = other.radius + this.radius;
        float px = other.center.x() - this.center.x() - vX;
        float py = other.center.y() - this.center.y() - vY;

        return ((r * r) >= (px * px + py * py));
    }


    @Override
    public boolean collidesWith(IBoundingBox other, float vX, float vY) {
        if(other instanceof CircleBB) {
            return this.collidesWith((CircleBB) other, vX, vY);
        }

        return false;
    }

    @Override
    public boolean contains(float x, float y, float vX, float vY) {
        return (Math.abs(this.center.x() + vX - x) <= this.radius) && (Math.abs(this.center.y() + vY - y) < this.radius);
    }

    @Override
    public boolean isOutsideBox(float bW, float bH, float vX, float vY) {
        return (this.center.x() + vX + radius < 0) || (this.center.x() + vX - radius > bW)
                || (this.center.y() + vY - radius > bH) || (this.center.y() + vY + radius < 0);
    }

    @Override
    public boolean collidesWithBox(float bW, float bH, float vX, float vY) {
        return (this.center.x() + vX - radius <= 0) || (this.center.x() + vX + radius >= bW)
                || (this.center.y() + vY + radius >= bH) || (this.center.y() + vY - radius <= 0);
    }

    @Override
    public Vector2 getOrigin() {
        return this.center;
    }
}
