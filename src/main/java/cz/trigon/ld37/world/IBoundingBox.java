package cz.trigon.ld37.world;

import cz.dat.gaben.util.Vector2;

public interface IBoundingBox<T> {
    T mix(T other, float m);

    boolean collidesWith(T other, float vX, float vY);
    default boolean collidesWith(T other) {
        return this.collidesWith(other, 0, 0);
    }

    boolean collidesWith(IBoundingBox other, float vX, float vY);
    default boolean collidesWith(IBoundingBox other) {
        return this.collidesWith(other, 0, 0);
    }

    boolean contains(float x, float y, float vX, float vY);
    default boolean contains(float x, float y) {
        return this.contains(x, y, 0, 0);
    }

    boolean isOutsideBox(float bW, float bH, float vX, float vY);
    default boolean isOutsideBox(float bW, float bH) {
        return this.isOutsideBox(bW, bH, 0, 0);
    }

    boolean collidesWithBox(float bW, float bH, float vX, float vY);
    default boolean collidesWithBox(float bW, float bH) {
        return this.collidesWithBox(bW, bH, 0, 0);
    }

    Vector2 getOrigin();
}
