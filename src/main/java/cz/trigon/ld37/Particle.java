package cz.trigon.ld37;

import cz.dat.gaben.api.interfaces.ITickListener;
import cz.dat.gaben.util.Color;

public class Particle implements ITickListener {
    
    private float lastPosX;
    private float lastPosY;
    
    private float friction;
    
    private float posX;
    private float posY;
    
    private float velX;
    private float velY;
    
    private int color;

    private int startLifetime;
    private int lifetime;
    private boolean dead;

    private int fadein;
    private int fadeout;
    
    public Particle(float x, float y, float velX, float velY, float friction, int lifetime, int color) {
        this(x, y, velX, velY, friction, lifetime, color, 0, 0);
    }

    public Particle(float x, float y, float velX, float velY, float friction, int lifetime, int color, int fadein, int fadeout) {
        this.posX = x;
        this.posY = y;

        this.lastPosX = x;
        this.lastPosY = y;

        this.velX = velX;
        this.velY = velY;
        this.color = color;
        this.friction = friction;
        this.startLifetime = lifetime;
        this.lifetime = lifetime;
        this.dead = false;

        this.fadein = fadein;
        this.fadeout = fadeout;
    }

    public float getPosX() {
        return this.posX;
    }

    public float getPosY() {
        return this.posY;
    }

    public int getColor() {
        return this.color;
    }
    
    public float getPartialX(float ptt) {
        return this.lastPosX + (this.posX - this.lastPosX) * ptt;
    }
    
    public float getPartialY(float ptt) {
        return this.lastPosY + (this.posY - this.lastPosY) * ptt;
    }
    
    public boolean isDead() {
        return this.dead;
    }

    public int getLifetime() { return this.lifetime; }

    public int getStartLifetime() { return this.startLifetime; }

    public int getFadein() { return this.fadein; }

    public int getFadeout() { return this.fadeout; }

    @Override
    public void tick() {
        this.lastPosX = this.posX;
        this.lastPosY = this.posY;
        
        this.posX += this.velX;
        this.posY += this.velY;
        
        this.velX *= this.friction;
        this.velY *= this.friction;
        
        this.lifetime--;
        
        if (this.lifetime <= 0) {
            this.dead = true;
        }
    }

    @Override
    public void renderTick(float ptt) {

    }    
    
}
