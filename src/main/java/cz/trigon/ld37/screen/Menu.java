package cz.trigon.ld37.screen;

import cz.dat.gaben.api.game.ScreenBase;
import cz.dat.gaben.api.interfaces.IFont;
import cz.dat.gaben.api.interfaces.IFontRenderer;
import cz.dat.gaben.api.interfaces.IInputManager;
import cz.dat.gaben.api.interfaces.IRenderer;
import cz.dat.gaben.data.IDataProvider;
import cz.dat.gaben.util.Anchor;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.Vector2;
import cz.trigon.ld37.LDGame;

import java.util.List;
import java.util.Random;


public class Menu extends ScreenBase {

    private class MenuItem {
        public String text;
        public boolean enabled;
        public Runnable onPress;

        public MenuItem(String text, Runnable onPress) {
            this.text = text;
            this.onPress = onPress;
            this.enabled = true;
        }
    }

    private IRenderer r;
    private IFontRenderer fontRenderer;
    private IFont font;
    private MenuItem[] items;
    private int selectedItem;
    private Random rnd = new Random();
    private int aCol = Color.WHITE;
    private LDGame game;

    public Menu(LDGame game) {
        super(game, "Menu");

        this.game = game;
        this.r = game.getApi().getRenderer();
        this.fontRenderer = game.getApi().getFontRenderer();
        this.font = game.getApi().getFont().getFont("menuFont");

        this.r = game.getApi().getRenderer();

        this.items = new MenuItem[4];
        this.items[0] = new MenuItem("Play game", () -> game.openScreen(1));
        this.items[1] = new MenuItem("Reset game", () -> {
            this.game.resetGame();
        });
        this.items[2] = new MenuItem("About", () -> game.openScreen(3));
        this.items[3] = new MenuItem("Exit", game::exit);

        this.items[1].enabled = false;
    }

    public void setPlayGameState(boolean play) {
        if (play) {
            this.items[0].text = "Play game";
            this.items[1].enabled = false;
        } else {
            this.items[0].text = "Continue game";
            this.items[1].enabled = true;
        }
    }

    @Override
    public void tick() {

    }

    @Override
    public void renderTick(float ptt) {
        this.fontRenderer.setFont(this.font);
        this.fontRenderer.setAnchor(Anchor.CENTER_CENTER);
        this.fontRenderer.setSize(72);
        this.r.color(Color.WHITE);
        this.fontRenderer.drawString(LDGame.TITLE, this.game.getWidth() / 2,
                this.fontRenderer.getMaxHeight() + 50);

        float y = this.fontRenderer.getMaxHeight() * 2 + 130;
        this.fontRenderer.setSize(48);
        for (int i = 0; i < this.items.length; i++) {
            if (this.items[i].enabled) {
                this.r.color(Color.WHITE);
            } else {
                this.r.color(Color.color(0.3f, 0.3f, 0.3f, 1f));
            }
            this.fontRenderer.drawString(this.items[i].text, this.game.getWidth() / 2, y);

            if (i == this.selectedItem) {
                this.r.color(Color.WHITE);
                this.r.enableTexture(false);
                this.r.shapeMode(IRenderer.ShapeMode.FILLED);
                this.r.drawTriangle(200, y - 10, 220, y, 200, y + 10);
                this.r.drawTriangle(this.game.getWidth() - 200, y - 10, this.game.getWidth() - 200, y + 10,
                        this.game.getWidth() - 220, y);
            }

            y += this.fontRenderer.getMaxHeight() + 15;
        }

        y += 25 + this.fontRenderer.getMaxHeight();
        this.fontRenderer.setSize(32);
        this.r.color(this.aCol);
        this.fontRenderer.drawString("Use scroll wheel to change the volume.",
                this.game.getWidth() / 2, y);
        y += this.fontRenderer.getMaxHeight();

        this.fontRenderer.drawString("Controls: mouse, LMB for placing a tower (1000 points)",
                this.game.getWidth() / 2, y);
        y +=  this.fontRenderer.getMaxHeight();

        int ml = this.game.getData().getData("maxLevel", Integer.class);

        if (ml > 0) {
            this.fontRenderer.setSize(24);
            this.fontRenderer.drawString("Your greatest life achievement is level " + ml + ".",
                    this.game.getWidth() / 2, y);
        }

        this.r.enableTexture(false);
        this.r.color(Color.WHITE);
        this.r.shapeMode(IRenderer.ShapeMode.FILLED);
        this.r.drawRect(30, 30, 45, 30 + (this.game.getHeight() - 60) *
                this.game.getData().getData("masterVolume", Float.class));
    }

    @Override
    public void onOpening() {
        super.onOpening();
        this.game.getApi().getSound().stopAllSounds();
        this.game.getApi().getSound().playMusic("menu", 0.7f, true, true);
    }

    @Override
    public void onMouseScroll(float x, float y) {
        super.onMouseScroll(x, y);

        IDataProvider p = this.game.getData();
        float vol = p.getData("masterVolume", Float.class);
        vol += 0.05 * y;
        if(vol > 1f) vol = 1f;
        if(vol < 0) vol = 0;
        p.setData("masterVolume", vol, Float.class);

        this.game.getApi().getSound().setGain(vol);
    }

    private int mouseSelectedItem = -1;

    @Override
    public void onMousePositionChange(float x, float y) {
        super.onMousePositionChange(x, y);
        this.fontRenderer.setFont(this.font);
        this.fontRenderer.setSize(48);

        float itemHeight = this.fontRenderer.getMaxHeight() + 15;
        int startY = 250;

        Vector2 mPos = this.unprojectCoordUI(x, y);
        // TODO: plz
        if(mPos.x() > 490 && mPos.x() < 785 && mPos.y() > startY && mPos.y() < (startY + itemHeight * this.items.length)) {
            this.selectedItem = (int)Math.floor((mPos.y() - startY) / itemHeight);
            this.mouseSelectedItem = this.selectedItem;
        } else {
            this.mouseSelectedItem = -1;
        }
    }

    @Override
    public void onMouseButtonDown(int button) {
        super.onMouseButtonDown(button);
        if(this.mouseSelectedItem != -1) {
            this.onKeyDown(IInputManager.Keys.ENTER);
        }
    }

    public Vector2 unprojectCoordUI(float x, float y) {
        x = x / this.game.getWidth();
        y = y / this.game.getHeight();

        x = 2.0f * x - 1f;
        y = 2.0f * y - 1f;

        float bx = x;
        float by = y;

        y *= 1f + ((bx / 4f) * (bx / 4f));
        x *= 1f + ((by / 5f) * (by / 5f));

        x *= 1.0585f;
        y *= 1.048f;

        x = (x + 1f) / 2;
        y = (y + 1f) / 2;

        x = x / 2f + 0.5f;
        y = y / 2f + 0.5f;

        x = (x - 0.5f) * this.game.getWidth() * 2;
        y = (y - 0.5f) * this.game.getHeight() * 2;

        return new Vector2(x, y);
    }

    @Override
    public void onKeyDown(int key) {
        super.onKeyDown(key);

        if (key == IInputManager.Keys.W || key == IInputManager.Keys.UP) {
            this.selectedItem--;
            if (this.selectedItem == -1)
                this.selectedItem = this.items.length - 1;

            this.game.getApi().getSound().playSoundAlways("menuMove");
        }

        if (key == IInputManager.Keys.S || key == IInputManager.Keys.DOWN) {
            this.selectedItem = (this.selectedItem + 1) % this.items.length;

            this.game.getApi().getSound().playSoundAlways("menuMove");
        }

        if (key == IInputManager.Keys.ENTER) {
            if (this.selectedItem == 0 && !this.game.getScreen(1).wasOpened()) {
                this.setPlayGameState(false);
            }

            if (this.items[this.selectedItem].enabled) {
                this.items[this.selectedItem].onPress.run();
                this.game.getApi().getSound().playSoundAlways("menuMove");
            } else {
                this.game.getApi().getSound().playSoundAlways("menuDenied", 2f, 1f);
            }
        }
    }
}
