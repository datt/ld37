package cz.trigon.ld37.screen;

import cz.dat.gaben.api.Settings;
import cz.dat.gaben.api.game.ScreenBase;
import cz.dat.gaben.api.interfaces.*;
import cz.dat.gaben.util.Anchor;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.LineRenderer;
import cz.dat.gaben.util.Vector2;
import cz.trigon.ld37.LDGame;
import cz.trigon.ld37.ParticleEngine;
import cz.trigon.ld37.entity.*;
import cz.trigon.ld37.level.Level;
import cz.trigon.ld37.world.CircleBB;

import java.util.*;
import java.util.stream.Stream;

public class Playground extends ScreenBase {
    private Random rnd = new Random();
    private LDGame game;

    private IRenderer r;
    private IFontRenderer fr;
    private IFont font;

    private LineRenderer lr;
    private ParticleEngine pe;

    private IFbo gameCanvasFbo, screenFbo;

    private int points = 0;

    private int ctick;
    private CircleBB towersBanned = new CircleBB(160, 1280/2, 720/2);

    int segsize = 20;
    int segsx = 64 + 1;
    int segsy = 36 + 1;

    private int lastHightlight = 5;
    private int hightlight = 5;

    class Vertex {
        public float x, y, lastX, lastY, velX, velY;
    }

    private Vertex[] data;

    private List<Entity> entities, entitiesToAdd, entitiesToRemove;

    private Comparator<Entity> entityPriorityComparator =
            (c1, c2) -> c1.getTickPriority() - c2.getTickPriority();

    private Level currentLevel;
    public int currentLevelNum;

    public Playground(LDGame game) {
        super(game, "Game");
        this.game = game;

        this.data = new Vertex[segsx * segsy];

        for (int x = 0; x < segsx; x++) {
            for (int y = 0; y < segsy; y++) {
                Vertex v = new Vertex();
                v.x = v.lastX = x * segsize;
                v.y = v.lastY = y * segsize;
                this.data[y * segsx + x] = v;
            }
        }

        Settings s = new Settings();
        s.setMinFilter(Settings.Filters.LINEAR);
        s.setMagFilter(Settings.Filters.LINEAR);
        game.getApi().getFbo().createFbo("gameField", game.getWidth(), game.getHeight(), 1, s);

        this.gameCanvasFbo = game.getApi().getFbo().getFbo("gameField");
        this.screenFbo = game.getApi().getFbo().getFbo("screen");

        this.r = game.getApi().getRenderer();
        this.fr = game.getApi().getFontRenderer();
        this.font = game.getApi().getFont().getFont("menuFont");
        this.lr = new LineRenderer(this.r, 500);
        this.pe = new ParticleEngine(game);

        this.entities = new ArrayList<>();
        this.entitiesToAdd = new ArrayList<>();
        this.entitiesToRemove = new ArrayList<>();

        this.addEntity(new Rift(this));
        this.entities.addAll(this.entitiesToAdd);
        this.entitiesToAdd.clear();

        this.nextLevel();
    }

    public Level getLevel() {
        return this.currentLevel;
    }

    public Vector2 unprojectCoord(Vector2 coord) {
        /*return this.unprojectCoord(coord, false);
    }

    public Vector2 unprojectCoord(Vector2 coord, boolean ui) {*/
        float x = coord.x();
        x = /*(ui ? x : */(1.04918f * x - 31.4754f) / this.game.getWidth();

        float y = coord.y();
        y = /*(ui ? y : */(1.09091f * y - 32.7273f) / this.game.getHeight();

        x = 2.0f * x - 1f;
        y = 2.0f * y - 1f;

        float bx = x;
        float by = y;

        y *= 1f + ((bx / 4f) * (bx / 4f));
        x *= 1f + ((by / 5f) * (by / 5f));

        x *= 1.0585f;
        y *= 1.048f;

        x = (x + 1f) / 2;
        y = (y + 1f) / 2;

        x = x / 2f + 0.5f;
        y = y / 2f + 0.5f;

        x = (x - 0.5f) * this.game.getWidth() * 2;
        y = (y - 0.5f) * this.game.getHeight() * 2;

        return new Vector2(x, y);
    }

    public ParticleEngine getParticleEngine() {
        return this.pe;
    }

    public int getPoints() {
        return this.points;
    }

    public int addPoints(int p) {
        this.points += p;
        return this.points;
    }

    public int removePoints(int p) {
        this.points -= p;
        return this.points;
    }

    public void addEntity(Entity e) {
        if (!this.entitiesToAdd.contains(e) && !this.entities.contains(e)) {
            this.entitiesToAdd.add(e);
            if (e instanceof EntityInputListener)
                this.game.getApi().getInput().addEventListener((EntityInputListener) e);
        }
    }

    public void removeEntity(Entity e) {
        if (!this.entitiesToRemove.contains(e) && this.entities.contains(e)) {
            this.entitiesToRemove.add(e);
            if (e instanceof EntityInputListener)
                this.game.getApi().getInput().removeEventListener((EntityInputListener) e);
        }
    }

    public <T extends Entity> Stream<T> getEntities(Class<T> type) {
        return this.entities.stream().filter(type::isInstance).map(e -> (T) e);
    }

    public void deformGrid(float x, float y, float force) {
        if (force != 0) {
            for (Vertex v : this.data) {
                float vx = v.x - x;
                float vy = v.y - y;
                float mag = (float) Math.sqrt(vx * vx + vy * vy);
                vx /= mag;
                vy /= mag;
                float distx = (v.x - x);
                float disty = (v.y - y);
                float dist = (float) Math.sqrt(distx * distx + disty * disty);
                float s = (100f / (dist + 100f)) * force;
                v.velX += vx * s;
                v.velY += vy * s;
            }
        }
    }

    private void nextLevel() {
        this.currentLevel = new Level(this, 15000 + (this.currentLevelNum * 4000),
                (35 - (int)((34f/20f) * this.currentLevelNum)), 3 + (this.currentLevelNum / 4), 200,
                0.045f,
                0.0001f+(this.currentLevelNum*0.0001f), 10 + this.currentLevelNum);
        this.currentLevelNum++;
    }

    @Override
    public void tick() {
        this.currentLevel.tick();
        if(this.currentLevel.hasStarted() && this.currentLevel.hasEnded()) {
            this.nextLevel();
        }

        this.lastHightlight = this.hightlight;

        if (this.currentLevel.hasEnded()) {
            this.hightlight++;
        } else {
            this.hightlight--;
        }

        if (this.hightlight < 0) {
            this.hightlight = 0;
        }

        if (this.hightlight > 5) {
            this.hightlight = 5;
        }

        this.ctick++;

        Vector2 mp = unprojectCoord(game.getApi().getInput().getMousePosition());
        this.deformGrid(mp.x(), mp.y(), 2);

        for (int x = 0; x < segsx; x++) {
            for (int y = 0; y < segsy; y++) {
                Vertex v = this.data[y * segsx + x];
                v.lastX = v.x;
                v.lastY = v.y;

                float nx = x * segsize;
                float ny = y * segsize;

                v.velX += (nx - v.x) * 0.625f;
                v.velY += (ny - v.y) * 0.625f;

                v.velX *= 0.65f;
                v.velY *= 0.65f;

                v.x += v.velX;
                v.y += v.velY;
            }
        }

        for (Entity e : this.entities) {
            e.tick();
            this.deformGrid(e.getBoundingBox().getOrigin().x(), e.getBoundingBox().getOrigin().y(), e.getWarpForce());
            if (e.isDead())
                this.entitiesToRemove.add(e);
        }

        if (this.entitiesToAdd.size() > 0) {
            this.entities.addAll(this.entitiesToAdd);
            this.entities.sort(this.entityPriorityComparator);
            this.entitiesToAdd.clear();
        }

        if (this.entitiesToRemove.size() > 0) {
            this.entities.removeAll(this.entitiesToRemove);
            this.entities.sort(this.entityPriorityComparator);
            this.entitiesToRemove.clear();
        }

        this.pe.tick();
    }

    @Override
    public void renderTick(float ptt) {
        this.r.fbo(this.gameCanvasFbo);
        this.r.defaultShader();
        this.r.clear();
        this.r.enableTexture(false);

        //this.r.setMatrix(this.r.identityMatrix().scaleFrom(this.game.getWidth() / 2, this.game.getHeight() / 2, 0.995f, 0.9925f));

        lr.width(2);

        int colorn = Color.color(0, 0.3f, 0.3f, 0.65f);
        int colorns = Color.color(0, 0.5f, 0.5f, 0.65f);
        int colore = Color.color(1, 0, 0, 0.65f);

        for (int x = 0; x < segsx; x++) {
            int nc = colorn;
            if (x % 4 == 0) nc = colorns;
            for (int y = 0; y < segsy; y++) {
                Vertex c = data[y * segsx + x];
                float rx = c.lastX + (c.x - c.lastX) * ptt;
                float ry = c.lastY + (c.y - c.lastY) * ptt;

                float nx = x * segsize;
                float ny = y * segsize;
                float distX = rx - nx;
                float distY = ry - ny;
                float dist = (float) Math.sqrt(distX * distX + distY * distY);

                float w = Math.abs(dist / 100f);

                int finalColor = Color.mix(nc, colore, w);

                lr.color(finalColor);
                lr.vertex(rx, ry);
            }
            lr.end(false);
        }

        for (int y = 0; y < segsy; y++) {
            int nc = colorn;
            if (y % 4 == 0) nc = colorns;
            for (int x = 0; x < segsx; x++) {
                Vertex c = data[y * segsx + x];
                float rx = c.lastX + (c.x - c.lastX) * ptt;
                float ry = c.lastY + (c.y - c.lastY) * ptt;

                float nx = x * segsize;
                float ny = y * segsize;
                float distX = rx - nx;
                float distY = ry - ny;
                float dist = (float) Math.sqrt(distX * distX + distY * distY);

                float w = Math.abs(dist / 100f);

                int finalColor = Color.mix(nc, colore, w);

                lr.color(finalColor);
                lr.vertex(rx, ry);
            }
            lr.end(false);
        }

        this.r.flush();

        this.r.setMatrix(this.r.identityMatrix());

        for (Entity e : this.entities) {
            e.renderGame(ptt);
        }

        this.pe.renderTick(ptt);

        if (!this.currentLevel.hasStarted() | this.lastHightlight > 0 | this.hightlight > 0) {
            float m = (this.lastHightlight + (this.hightlight - this.lastHightlight)*ptt) / 5f;
            Vector2 mp = unprojectCoord(this.game.getApi().getInput().getMousePosition());
            this.r.color((testTowerSpot() && this.points >= 1000) ? Color.GREEN : Color.RED);
            this.r.drawSegCircle(mp.x(), mp.y(), 30*m, 16);
            //this.r.shapeMode(IRenderer.ShapeMode.FILLED);
            this.r.color(Color.color(1f, 0.4f, 0f, 0.3f));
            this.r.drawSegCircle(this.towersBanned.center, this.towersBanned.radius*m, 32);
            for (Entity e : entities) {
                if (e instanceof Tower) {
                    this.r.drawSegCircle(e.getBoundingBox().getOrigin(), 100*m, 16);
                }
            }
        }

        this.r.fbo(this.screenFbo);
        this.r.passthroughShader();
        this.r.bindFboTexture(this.gameCanvasFbo, 0, 0);
        this.r.shapeMode(IRenderer.ShapeMode.FILLED);
        this.r.drawRect(30, 30, this.screenFbo.getWidth() - 30, this.screenFbo.getHeight() - 30);
        this.r.defaultShader();
        for (Entity e : this.entities) {
            e.renderUI(ptt);
        }

        this.fr.setFont(this.font);
        this.fr.setAnchor(Anchor.TOP_LEFT);
        this.fr.setSize(36);
        this.fr.drawString("Points: " + this.points, 45, 45);

        if(this.currentLevel.hasEnded()) {
            this.fr.setAnchor(Anchor.TOP_RIGHT);
            Optional<Rift> r = this.getEntities(Rift.class).findFirst();
            if (this.ctick % 25 < 20 && (!r.isPresent() || !r.get().isDying())) {
                this.fr.drawString("-- PRESS SPACE TO START L" + this.currentLevelNum + " --", this.game.getWidth() - 45, 45);
            }
        }
    }

    @Override
    public void onClosing() {
        super.onClosing();

        for (Entity e : this.entities) {
            if (e instanceof EntityInputListener) {
                this.game.getApi().getInput().removeEventListener((EntityInputListener) e);
            }
        }
    }

    @Override
    public void onOpening() {
        super.onOpening();
        this.game.getApi().getSound().playMusic("main", 1f, true, true);

        for (Entity e : this.entities) {
            if (e instanceof EntityInputListener) {
                this.game.getApi().getInput().addEventListener((EntityInputListener) e);
            }
        }
    }

    @Override
    public void onKeyDown(int key) {
        super.onKeyDown(key);

        if (key == IInputManager.Keys.ESCAPE) {
            this.game.openScreen(0);
        }

        if (key == IInputManager.Keys.SPACE) {
            this.currentLevel.startLevel();
        }
    }

    @Override
    public void onMouseButtonDown(int button) {
        super.onMouseButtonDown(button);

        if (button == 0 && !this.currentLevel.hasStarted()) {
            Vector2 pos = this.unprojectCoord(this.game.getApi().getInput().getMousePosition());
            CircleBB test = new CircleBB(80, pos);
            CircleBB test2 = new CircleBB(0.01f, pos);
            if (this.getEntities(Tower.class).filter(t -> t.getBoundingBox().collidesWith(test)).count() == 0 && !test2.collidesWith(this.towersBanned)) {
                if(this.points >= 1000) {
                    TowerSlowing t = new TowerSlowing(this, 25, pos.x(), pos.y()); // TODO
                    this.addEntity(t);
                    this.points -= 1000;
                } else {
                    this.game.getApi().getSound().playSound("menuDenied");
                }
            }
        }
    }

    public boolean testTowerSpot() {
        Vector2 pos = this.unprojectCoord(this.game.getApi().getInput().getMousePosition());
        CircleBB test = new CircleBB(80, pos);
        CircleBB test2 = new CircleBB(0.01f, pos);
        return (this.getEntities(Tower.class).filter(t -> t.getBoundingBox().collidesWith(test)).count() == 0 && !test2.collidesWith(this.towersBanned));
    }
}
