package cz.trigon.ld37;

import cz.dat.gaben.api.Settings;
import cz.dat.gaben.api.game.Game;
import cz.dat.gaben.api.game.ScreenBase;
import cz.dat.gaben.api.interfaces.IFbo;
import cz.dat.gaben.api.interfaces.IRenderer;
import cz.dat.gaben.api.interfaces.IShader;
import cz.dat.gaben.data.FileSystemDataStorage;
import cz.dat.gaben.data.IDataProvider;
import cz.dat.gaben.data.IDataStorage;
import cz.dat.gaben.exception.DataSyncException;
import cz.dat.gaben.util.Color;
import cz.trigon.ld37.screen.About;
import cz.trigon.ld37.screen.Menu;
import cz.trigon.ld37.screen.Playground;

import java.util.Random;

public class LDGame extends Game {

    public static final String TITLE = "Riftong";

    private IRenderer r;
    private IShader crtShader;
    private IFbo screenFbo;

    private float lastTransitionEffect = 50000.0f;
    private float transitionEffect = 50000.0f;

    Random rand = new Random();

    private float lastShakeX = 0.0f, lastShakeY = 0.0f;
    private float shakeX = 0.0f, shakeY = 0.0f;
    private float shakeIntensity = 0.0f;
    private long startTime;

    private ParticleEngine menuParticles;

    IDataProvider data;

    @Override
    public void init() {
        super.init();

        this.menuParticles = new ParticleEngine(this);

        this.getWindow().setTitle(LDGame.TITLE);

        this.getApi().getFont().loadFont("menuFont", "menuFont", 128);
        this.getApi().getTexture().loadTexture("czflag", "textures/cz");

        Settings s = new Settings();
        s.setMinFilter(Settings.Filters.LINEAR);
        s.setMagFilter(Settings.Filters.LINEAR);

        this.r = this.getApi().getRenderer();

        this.getApi().getFbo().createFbo("screen", this.getWidth(), this.getHeight(), 1, s);

        this.getApi().getShader().loadShader("crt", "crt_v", "crt_f");
        this.r.shader(this.getApi().getShader().getShader("crt"));
        this.r.setupShader(this.getApi().getShader().getShader("crt"), false);
        this.r.defaultShader();

        this.getApi().getTexture().finishLoading();

        this.getApi().getSound().addSound("menuMove", "sounds/menuMove");
        this.getApi().getSound().addSound("menuDenied", "sounds/ushallnotpass");

        this.getApi().getSound().addSound("explosion", "sounds/expl2");
        this.getApi().getSound().addSound("hit1", "sounds/hit1");
        this.getApi().getSound().addSound("hit2", "sounds/hit2");
        this.getApi().getSound().addSound("hit3", "sounds/hit3");


        this.getApi().getSound().addMusic("menu", "sounds/menu");
        this.getApi().getSound().addMusic("main", "sounds/main");

        this.getApi().getSound().finishLoading();

        this.screenFbo = this.getApi().getFbo().getFbo("screen");
        this.crtShader = this.getApi().getShader().getShader("crt");

        IDataStorage dataStorage = new FileSystemDataStorage("trigonLD37");
        dataStorage.load();
        this.data = dataStorage.createOrGet("config");
        dataStorage.syncAll();
        this.data.createDataIfNotPresent("masterVolume", 1f, Float.class);
        this.data.createDataIfNotPresent("maxLevel", 0, Integer.class);

        this.addScreen(0, new Menu(this));
        this.addScreen(1, new Playground(this));
        this.addScreen(3, new About(this));
        this.openScreen(0);

        this.getApi().getSound().setGain(this.data.getData("masterVolume", Float.class));

        this.startTime = System.nanoTime();
    }

    public IDataProvider getData() {
        return this.data;
    }

    public void resetGame() {
        this.openScreen(0);
        this.screens.remove(1);
        ((Menu)this.getScreen(0)).setPlayGameState(true);
        this.addScreen(1, new Playground(this));
    }

    @Override
    public void openScreen(ScreenBase s) {
        super.openScreen(s);

        this.transitionEffect += 1.6f;
    }

    @Override
    public void cleanup() {
        super.cleanup();
        try {
            this.data.sync(IDataProvider.MergeType.PREFER_MEMORY);
        } catch (DataSyncException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTick() {
        super.onTick();

        if (!(this.currentScreen instanceof Playground)) {
            for (int i = 0; i < 50; i++) {
                this.menuParticles.addParticle(new Particle(
                        this.rand.nextFloat() * this.getWidth(),
                        this.rand.nextFloat() * this.getHeight(),
                        (this.rand.nextFloat() - 0.5f) * 5,
                        (this.rand.nextFloat() - 0.5f) * 5,
                        0.985f,
                        10 + this.rand.nextInt(40),
                        Color.color(1f, 1f, 1f, 0.3f + rand.nextFloat() * 0.5f),
                        20,
                        20
                ));
            }

            this.menuParticles.tick();
        }

        this.lastShakeX = shakeX;
        this.lastShakeY = shakeY;

        this.shakeX = (rand.nextFloat() - 0.5f) * 2;
        this.shakeY = (rand.nextFloat() - 0.5f) * 2;

        if (this.shakeIntensity > 5.0f) {
            this.shakeIntensity *= 0.62f;
        } else {
            this.shakeIntensity *= 0.85f;
        }

        this.lastTransitionEffect = this.transitionEffect;
        this.transitionEffect *= 0.6f;
    }

    @Override
    public void onRenderTick(float ptt) {
        this.r.clearColor(Color.BLACK);
        this.r.clear();
        this.r.fbo(this.screenFbo);
        this.r.defaultShader();
        this.r.clearColor(0.1f, 0.1f, 0.1f, 1f);

        this.r.clear();

        if (!(this.currentScreen instanceof Playground)) {
            this.r.enableTexture(false);
            this.menuParticles.renderTick(ptt);
            this.r.flush();
        }

        super.onRenderTick(ptt);

        this.r.fbo(null);

        this.r.shader(this.crtShader);
        this.crtShader.setUniform1f("time", (float) ((System.nanoTime() - this.startTime) / 1000000000d));
        this.crtShader.setUniform2f("screenSize", this.getWidth(), this.getHeight());

        float sx = (this.lastShakeX + (this.shakeX - this.lastShakeX) * ptt) * this.shakeIntensity;
        float sy = (this.lastShakeY + (this.shakeY - this.lastShakeY) * ptt) * this.shakeIntensity;
        this.crtShader.setUniform2f("shake", sx, sy);

        this.crtShader.setUniform1f("transitionEffect", this.lastTransitionEffect + (this.transitionEffect - this.lastTransitionEffect) * ptt);
        this.crtShader.setUniform1f("distortion", 0.3f); // TODO

        this.r.bindFboTexture(this.screenFbo, 0, 0);
        this.r.shapeMode(IRenderer.ShapeMode.FILLED);
        this.r.drawFullscreenQuad();
    }

    public void shake(float intensity) {
        this.shakeIntensity += intensity;
    }


}
