package cz.trigon.ld37.level;

import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.Vector2;
import cz.trigon.ld37.LDGame;
import cz.trigon.ld37.entity.Bullet;
import cz.trigon.ld37.entity.Entity;
import cz.trigon.ld37.entity.Rift;
import cz.trigon.ld37.entity.Tower;
import cz.trigon.ld37.screen.Playground;
import cz.trigon.ld37.world.CircleBB;

import java.util.Random;

public class Level {

    protected Playground playground;
    protected LDGame game;
    protected Random rnd = new Random();

    protected int levelTime;
    protected int startTime;
    protected boolean started;

    protected Rift entRift;

    private int levelTimeMillis;
    private int spawnRate;
    private int maxBunch;
    private int pointsPerBullet;
    private float damageRate;
    private float regenRate;
    private float maxVel;

    public Level(Playground p, int levelTimeMillis, int spawnRate, int maxBunch, int pointsPerBullet, float damageRate,
                 float regenRate, float maxVel) {
        this.playground = p;
        this.game = (LDGame) p.getGame();
        this.levelTime = levelTimeMillis;

        this.spawnRate = spawnRate;
        this.maxBunch = maxBunch;
        this.pointsPerBullet = pointsPerBullet;
        this.damageRate = damageRate;
        this.regenRate = regenRate;
        this.maxVel = maxVel;

        this.entRift = p.getEntities(Rift.class).findFirst().get();
    }

    public void startLevel() {
        if (!this.started && !this.entRift.isDying()) {
            this.started = true;
            this.startTime = (int) (System.nanoTime() / 1000000);
        }
    }

    public void tick() {
        if (this.started) {
            if (this.hasEnded()) {
                //this.started = false;

                this.playground.getEntities(Bullet.class).forEach(b -> {
                    int particleColor = Color.color(1f, 1f, 0f, 1f);
                    this.playground.getParticleEngine().spawnParticleCloud(5, 7, 25, b.getBoundingBox().getOrigin(),
                          particleColor, 0.0f, 2f, 20);

                    this.playground.deformGrid(b.getBoundingBox().getOrigin().x(), b.getBoundingBox().getOrigin().y(), 50);
                    b.setDead();
                    this.playRandomHitSound();

                });

                this.game.shake(8f);
            } else {
                this.spawnEnemies();
                this.checkEnemyCollisions();
            }
        }

        if (this.entRift.isDead()) {
            int cur = this.game.getData().getData("maxLevel", Integer.class);
            int now = this.playground.currentLevelNum-1;

            if (now > cur) {
                this.game.getData().setData("maxLevel", now, Integer.class);
            }

            this.game.resetGame();
        }
    }

    public float getRegenRate() {
        return regenRate;
    }

    public int getPointsPerBullet() {
        return pointsPerBullet;
    }

    public int getLevelTime() {
        return this.levelTime;
    }

    public int getTimeLeft() {
        return this.startTime + this.levelTime - (int) (System.nanoTime() / 1000000);
    }

    public boolean hasStarted() {
        return this.started;
    }

    public boolean hasEnded() {
        return this.getTimeLeft() <= 0;
    }

    private void makeCollisionParticles(Entity first, Entity second, int color) {
        float angle = (float) Math.atan2(first.getBoundingBox().getOrigin().x()
                        - second.getBoundingBox().getOrigin().x(),
                second.getBoundingBox().getOrigin().y() - first.getBoundingBox().getOrigin().y());
        angle = (float) Math.toDegrees(angle);


        this.playground.getParticleEngine().spawnParticleCloudArc(angle, 10, 0, 20, 100,
                second.getBoundingBox().getOrigin(),
                color, 0.0f, ((CircleBB) second.getBoundingBox()).radius, 20);
        this.playground.getParticleEngine().spawnParticleCloud(5, 7, 25, first.getBoundingBox().getOrigin(),
                Color.color(1f, 1f, 0f, 1f), 0.0f, 2f, 20);
    }

    private void playRandomHitSound() {
        this.game.getApi().getSound().playSound("hit" + (this.rnd.nextInt(3) + 1), 1f, 0.5f + rnd.nextFloat());
    }

    protected void basicRiftCollisionCheck(Class<? extends Entity> entity, float dmg) {
        this.playground.getEntities(entity).forEach(b -> {
            if (b != this.entRift && this.entRift.getBoundingBox().collidesWith(b.getBoundingBox())) {
                int particleColor = Color.color(1f - this.entRift.getHealthLeft(), 0.5f * this.entRift.getHealthLeft(),
                        0.5f * this.entRift.getHealthLeft(), 1f);
                this.makeCollisionParticles(b, this.entRift, particleColor);

                this.playground.deformGrid(b.getBoundingBox().getOrigin().x(), b.getBoundingBox().getOrigin().y(), 50);
                this.entRift.hurt(dmg);
                this.game.shake(4f);
                b.setDead();
                this.playRandomHitSound();
            }
        });
    }

    protected void basicTowersCollisionCheck(Class<? extends Entity> entity, float dmg) {
        this.playground.getEntities(Tower.class).forEach(t -> {
            this.playground.getEntities(entity).forEach(b -> {
                if (t.getBoundingBox().collidesWith(b.getBoundingBox())) {
                    this.makeCollisionParticles(b, t, Color.RED);

                    this.playground.deformGrid(b.getBoundingBox().getOrigin().x(), b.getBoundingBox().getOrigin().y(), 50);
                    t.hurt(t.getDamageMult() * dmg);
                    if(t.isDead()) {
                        this.playground.getParticleEngine().spawnParticleCloud(((CircleBB)t.getBoundingBox()).radius,
                                25, 250, t.getBoundingBox().getOrigin(), Color.RED, 0.1f, 0, 15);
                    }
                    this.game.shake(4f);
                    b.setDead();
                    this.playRandomHitSound();
                }
            });
        });
    }

    protected void basicShieldCollisionCheck(Class<? extends Entity> entity, float dmg) {
        this.playground.getEntities(entity).forEach(b -> {
            float r = ((CircleBB) b.getBoundingBox()).radius;
            Vector2 bPos = b.getBoundingBox().getOrigin();
            Vector2 riftPos = this.entRift.getBoundingBox().getOrigin();

            float xDiff = bPos.x() - riftPos.x();
            float yDiff = riftPos.y() - bPos.y();

            float dist = bPos.getDistance(riftPos);

            float bulletAngle = (float) Math.toDegrees(Math.atan2(xDiff, yDiff));
            float paddleAngle = this.entRift.getPaddleAngle();

            float angleDiff = Math.abs((paddleAngle - bulletAngle + 180 + 360) % 360 - 180);

            boolean correctAngle = (angleDiff < Rift.PADDLE_ANGLE_SIZE / 2);
            boolean correctDistance = (dist > (Rift.PADDLE_OFFSET - r - 5)) && (dist < (Rift.PADDLE_OFFSET + r));

            if (correctAngle && correctDistance) {
                this.playground.getParticleEngine().spawnParticleCloudArc(bulletAngle, 5, 0, 10, 40,
                        riftPos,
                        Color.color(1f, 1f, 0f, 1f), 0.0f, Rift.PADDLE_OFFSET, 20);
                this.playground.getParticleEngine().spawnParticleCloud(5, 7, 25, bPos,
                        Color.color(1f, 1f, 0f, 1f), 0.0f, 2f, 20);

                this.game.shake(2.5f);

                this.playground.deformGrid(b.getBoundingBox().getOrigin().x(), b.getBoundingBox().getOrigin().y(), 50);
                b.setDead();
                this.playground.addPoints(this.pointsPerBullet);
                this.playRandomHitSound();
            }
        });
    }

    protected void spawnEnemies() {
        if (this.game.getWindow().getPassedTicks() % this.spawnRate == 0) {
            int bunch = this.rnd.nextInt(this.maxBunch) + 1;
            for (int i = 0; i < bunch; i++) {
                boolean hor = rnd.nextBoolean();
                boolean rev = rnd.nextBoolean();
                float vel = (this.maxVel / 2) * (1f + this.rnd.nextFloat());

                float x;
                float y;

                if (hor) {
                    x = rnd.nextInt(this.game.getWidth() + 80) - 40;
                    y = rev ? -40 : this.game.getHeight() + 40;
                } else {
                    y = rnd.nextInt(this.game.getHeight() + 80) - 40;
                    x = rev ? -40 : this.game.getWidth() + 40;
                }

                float tX = 1280 / 2;
                float ty = 720 / 2;

                float diffX = -x + tX;
                float diffY = -y + ty;

                float mag = (float) Math.sqrt(diffX * diffX + diffY * diffY);

                diffX /= mag;
                diffY /= mag;

                diffX *= vel;
                diffY *= vel;

                Bullet b = new Bullet(this.playground, x, y, diffX, diffY);

                this.playground.addEntity(b);
            }
        }
    }

    protected void checkEnemyCollisions() {
        this.basicRiftCollisionCheck(this.entRift.isDying() ? Entity.class : Bullet.class, this.damageRate);
        this.basicShieldCollisionCheck(Bullet.class, 0.0f);
        this.basicTowersCollisionCheck(Bullet.class, this.damageRate * 2);
    }
}
