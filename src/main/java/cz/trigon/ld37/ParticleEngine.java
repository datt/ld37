package cz.trigon.ld37;

import cz.dat.gaben.api.interfaces.IRenderer;
import cz.dat.gaben.api.interfaces.ITickListener;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.Vector2;

import java.util.Random;

public class ParticleEngine implements ITickListener {

    private Particle[] particles;

    private int maxParticles;

    private int pCount;
    private int startPosition;

    private Random rnd;
    private LDGame context;

    public ParticleEngine(LDGame context) {
        this.particles = new Particle[(this.maxParticles = 32768)];
        
        this.context = context;
        
        this.pCount = 0;
        this.startPosition = 0;
        this.rnd = new Random();
    }

    public void spawnParticleCloud(float radius, float vel, int count, Vector2 center, int color, float colorFuzz, float minRad, int lifetime) {
        for (int i = 0; i < count; i++) {
            float velocity = this.rnd.nextFloat() * vel;

            double dir = rnd.nextDouble() * Math.PI * 2;
            float velX = (float) (Math.sin(dir)) * velocity;
            float velY = (float) (-Math.cos(dir)) * velocity;

            float offset = minRad + this.rnd.nextFloat() * radius * 0.3f;

            float startOffsetX = ((float) (Math.sin(dir))) * offset;
            float startOffsetY = ((float) (-Math.cos(dir))) * offset;

            float colorOffset = 1.0f - colorFuzz * rnd.nextFloat();

            int c = Color.color(colorOffset * Color.rf(color), colorOffset * Color.gf(color), colorOffset * Color.bf(color), 1f);

            int lt = lifetime + this.rnd.nextInt(lifetime);

            Particle p = new Particle(center.x() + startOffsetX, center.y() + startOffsetY, velX, velY, 1.0f, lt, c, 0, lt / 4);
            this.addParticle(p);
        }
    }

    public void spawnParticleCloudArc(float aCenter, float aRad, float radius, float vel, int count, Vector2 center, int color, float colorFuzz, float minRad, int lifetime) {
        for (int i = 0; i < count; i++) {
            float velocity = this.rnd.nextFloat() * vel;

            double dir = Math.toRadians(aCenter + (this.rnd.nextFloat() - 0.5f) * aRad * 2f);

            float velX = (float) (Math.sin(dir)) * velocity;
            float velY = (float) (-Math.cos(dir)) * velocity;

            float offset = minRad + this.rnd.nextFloat() * radius * 0.3f;

            float startOffsetX = ((float) (Math.sin(dir))) * offset;
            float startOffsetY = ((float) (-Math.cos(dir))) * offset;

            float colorOffset = 1.0f - colorFuzz * rnd.nextFloat();

            int c = Color.color(colorOffset * Color.rf(color), colorOffset * Color.gf(color), colorOffset * Color.bf(color), 1f);

            int lt = lifetime + this.rnd.nextInt(lifetime);

            Particle p = new Particle(center.x() + startOffsetX, center.y() + startOffsetY, velX, velY, 1.0f, lt, c, 0, lt / 4);
            this.addParticle(p);
        }
    }

    public void addParticle(Particle p) {
        if(this.pCount < this.maxParticles) {
            this.pCount++;
        } else {
            this.startPosition++;
            this.startPosition %= this.maxParticles;
        }

        int putPosition = (this.startPosition + (this.pCount - 1)) % this.maxParticles;
        
        particles[putPosition] = p;
    }

    @Override
    public void tick() {
        if(this.pCount > 0) {
            int endPosition = endPosition();
            int movePosition = this.startPosition;

            for(int currentPosition = this.startPosition; currentPosition != endPosition; currentPosition++) {
                currentPosition %= this.maxParticles;
                Particle p = particles[currentPosition];
                p.tick();
                if(!p.isDead()) {
                    this.particles[movePosition] = p;
                    movePosition++;
                    movePosition %= this.maxParticles;
                } else {
                    this.pCount--;
                }
            }
        }
    }

    @Override
    public void renderTick(float ptt) {
        if(this.pCount > 0) {
            IRenderer r = this.context.getApi().getRenderer();
            r.flush();
            
            int start = this.startPosition;
            int end = endPosition();
            
            r.pointSize(2);
            
            for(int i = start; i != end; i++) {
                i %= this.maxParticles;
                Particle p = particles[i];

                int col = p.getColor();

                if (p.getFadein() > 0 || p.getFadeout() > 0) {
                    float toEnd = p.getLifetime() / (float) p.getFadeout();
                    float sinceStart = (p.getStartLifetime() - p.getLifetime()) / (float) p.getFadein();

                    float min = Math.min(toEnd, sinceStart);

                    if (min < 1f) {
                        float a = Color.af(col);
                        a *= min;
                        int ao = ((int)(a*255f));
                        col = Color.color(Color.r(col), Color.g(col), Color.b(col), ao);
                    }
                }

                float x = p.getPartialX(ptt);
                float y = p.getPartialY(ptt);
                
                r.color(col);
                r.drawPoint(x, y);
            }
            
        }
    }

    public int getCount() {
        return this.pCount;
    }

    private int endPosition() {
        if(this.pCount == this.maxParticles) {
            return (this.startPosition - 1) == -1 ? this.maxParticles
                    : (this.startPosition - 1);
        } else {
            return ((this.startPosition + this.pCount) % (this.maxParticles + 1));
        }
    }
    
}
