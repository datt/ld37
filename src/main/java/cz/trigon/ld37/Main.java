package cz.trigon.ld37;

import cz.dat.gaben.api.game.GameWindowBase;
import cz.dat.gaben.api.game.GameWindowFactory;
import cz.dat.gaben.content.IContentManager;
import cz.dat.gaben.content.PackedContentManager;
import cz.dat.gaben.util.GabeLogger;

public class Main {
    public static void main(String[] args) {
        GabeLogger.init(Main.class);
        GameWindowFactory.enableSplashscreen(false, null);

        IContentManager content = new PackedContentManager("/resources", true);
        LDGame game = new LDGame();

        GameWindowBase b = GameWindowFactory.createGameOrDie(GameWindowFactory.Api.LWJGL, 1280, 720,
                content, true, game);
        b.start(false);
    }
}
