package cz.trigon.ld37.entity;

import cz.dat.gaben.api.interfaces.IFont;
import cz.dat.gaben.api.interfaces.IRenderer;
import cz.dat.gaben.util.Anchor;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.Vector2;
import cz.trigon.ld37.screen.Playground;
import cz.trigon.ld37.world.CircleBB;
import cz.trigon.ld37.world.IBoundingBox;

import java.util.Random;

public class Rift extends EntityInputListener {

    public static final int PADDLE_OFFSET = 110;
    public static final int PADDLE_ANGLE_SIZE = 40;
    public static final int DEATH_TICKS = 80;

    private float rotation, lastRotation;
    private Vector2 centerPos;
    private CircleBB bb;

    private Random rand;

    private float r = 50, rO = 4;

    private float lastPaddleAngle = 0f;
    private float paddleAngle = 0f;

    private float healthLeft;

    private int deathTicks = DEATH_TICKS;
    private boolean dead = false;
    private boolean dying = false;

    private IFont font;

    public Rift(Playground p) {
        super(p);
        this.rand = new Random();
        this.centerPos = new Vector2(this.game.getWidth() / 2,
                this.game.getHeight() / 2);
        this.bb = new CircleBB(r - rO, this.centerPos.x(), this.centerPos.y());
        this.priority = 1000;
        this.healthLeft = 1.0f;

        this.font = p.getGame().getApi().getFont().getFont("menuFont");
    }

    @Override
    public void setDead() {
        this.healthLeft = 0;
    }

    public float getHealthLeft() {
        return this.healthLeft;
    }

    public void hurt(float l) {
        this.healthLeft -= l;
        if (this.healthLeft < 0)
            this.healthLeft = 0;
    }

    @Override
    public void tick() {
        this.lastRotation = this.rotation;

        if (this.healthLeft <= 0f && !dying) {
            this.dying = true;
            this.game.getApi().getSound().playSoundAlways("explosion");

            this.playground.getParticleEngine().spawnParticleCloud(this.r, 25, 20 + (int) (250), this.centerPos, Color.RED, 0.1f, 0, 15);
            this.playground.getParticleEngine().spawnParticleCloud(0, 25, 20 + (int) (250), this.centerPos, Color.RED, 0.1f, 0, 15);
        }

        if (this.dying) {
            float id = 1 - (this.deathTicks / ((float) Rift.DEATH_TICKS));

            this.game.shake(8f * id);

            this.r += 40*id;
            ((CircleBB)this.getBoundingBox()).radius += 40*id;

            this.playground.getParticleEngine().spawnParticleCloud(0, 5f + 15*id, 20 + (int) (200 * id), this.centerPos, Color.RED, 0.1f, this.r, 20);
            this.playground.getParticleEngine().spawnParticleCloud(0, -5f - 15*id, 20 + (int) (200 * id), this.centerPos, Color.RED, 0.1f, this.r, 20);

            for (int i = 0; i < id * 30; i++) {
                float prem = rand.nextBoolean() ? 1 : -1;
                this.playground.deformGrid(this.rand.nextFloat()*1280, this.rand.nextFloat()*720, id * prem * 80);
            }

            this.deathTicks--;
            if (this.deathTicks <= 0) {
                this.dead = true;
            }
        }

        float ih = 1 - this.healthLeft;
        this.rotation += 1.5f + 5*ih;

        int c = Color.color(1f - this.healthLeft, 0.5f * this.healthLeft, 0.5f * this.healthLeft, 1f);

        Vector2 mousePos = this.game.getApi().getInput().getMousePosition();
        Vector2 riftPos = this.getBoundingBox().getOrigin();

        float xDiff = mousePos.x() - riftPos.x();
        float yDiff = riftPos.y() - mousePos.y();

        this.lastPaddleAngle = this.paddleAngle;
        this.paddleAngle = (float) Math.toDegrees(Math.atan2(xDiff, yDiff));

        float randx = (this.rand.nextFloat() - 0.5f) * ih * 70f;
        float randy = (this.rand.nextFloat() - 0.5f) * ih * 70f;

        this.playground.deformGrid(this.centerPos.x() + randx, this.centerPos.y() + randy, -10 - (ih * 10));

        this.playground.getParticleEngine().spawnParticleCloud(0, 5f + 5*ih, 7 + (int) (25 * ih), this.centerPos, c, 0.1f, this.r, 20);
        this.playground.getParticleEngine().spawnParticleCloud(0, -1.5f - 0.5f*ih, 2 + (int) (20 * ih), this.centerPos, c, 0.1f, this.r, 15);

        if(this.playground.getLevel().hasStarted()) {
            this.healthLeft += this.playground.getLevel().getRegenRate();
        }

        if(this.healthLeft > 1f)
            this.healthLeft = 1f;
    }

    @Override
    public void renderGame(float ptt) {

        float deltaRot = this.rotation - this.lastRotation;
        float rot = this.lastRotation + deltaRot * ptt;

        this.render.enableTexture(false);

        float d = this.deathTicks / ((float) Rift.DEATH_TICKS);
        float id = 1 - d;

        this.render.shapeMode(IRenderer.ShapeMode.FILLED);
        this.render.color(Color.color((1f - this.healthLeft) / 3, (0.5f * this.healthLeft) / 3, (0.5f * this.healthLeft) / 3, 1f * d));
        this.render.drawCircle(this.centerPos, this.r - 5);
        this.render.shapeMode(IRenderer.ShapeMode.OUTLINE);

        this.render.color(Color.color(1f - this.healthLeft, 0.5f * this.healthLeft, 0.5f * this.healthLeft, 1f * d));

        for (int i = 0; i < (5*d); i++) {
            this.render.setMatrix(this.render.identityMatrix().rotateFrom(this.centerPos.x(), this.centerPos.y(), rot * (1 + i / 5f)));
            this.render.drawSegCircle(this.centerPos, this.r - i * 3, 10 - i);
        }

        for (int i = 0; i < (5*d); i++) {
            this.render.color(Color.color(1f - this.healthLeft, 0.5f * this.healthLeft, 0.5f * this.healthLeft, 1f - (i / 5f)));
            this.render.setMatrix(this.render.identityMatrix().rotateFrom(this.centerPos.x(), this.centerPos.y(), rot * (1 + i / 5f)));
            this.render.drawSegCircle(this.centerPos, this.r + i * 3, 11 + i);
        }

        this.render.setMatrix(this.render.identityMatrix());


            float da = this.paddleAngle - this.lastPaddleAngle;

            float smoothPaddleAngle;
            if (Math.abs(da) > 180f) {
                if (this.lastPaddleAngle < this.paddleAngle) {
                    da = (this.lastPaddleAngle + 360f) - this.paddleAngle;
                    smoothPaddleAngle = this.lastPaddleAngle - da * ptt;
                } else {
                    da = this.lastPaddleAngle - (this.paddleAngle + 360f);
                    smoothPaddleAngle = this.lastPaddleAngle - da * ptt;
                }
            } else {
                smoothPaddleAngle = this.lastPaddleAngle + da * ptt;
            }

            this.render.color(Color.color(1f - this.healthLeft, 0.5f * this.healthLeft, 0.5f * this.healthLeft, 1f));
            float startAngle = smoothPaddleAngle - Rift.PADDLE_ANGLE_SIZE / 2f;

            for (int i = 0; i < 15 * d; i++) {
                this.render.drawSegArc(this.centerPos, Rift.PADDLE_OFFSET - i * 0.5f*(1f/d), startAngle - 90f, Rift.PADDLE_ANGLE_SIZE, 32);
            }

    }

    @Override
    public void renderUI(float ptt) {
        if (!this.dying) {
            this.render.color(Color.color(1, 1, 1, 0.55f));

            this.game.getApi().getFontRenderer().setFont(this.font);
            this.game.getApi().getFontRenderer().setAnchor(Anchor.CENTER_CENTER);
            this.game.getApi().getFontRenderer().setSize(20);
            this.game.getApi().getFontRenderer().drawString((int) (this.healthLeft * 100) + "%", this.centerPos);
        }
    }


    @Override
    public IBoundingBox getBoundingBox() {
        return this.bb;
    }

    @Override
    public boolean isDead() {
        return this.dead;
    }

    public boolean isDying() {
        return this.dying;
    }

    public float getPaddleAngle() {
        return this.paddleAngle;
    }
}

