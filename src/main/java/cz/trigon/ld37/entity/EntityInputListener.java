package cz.trigon.ld37.entity;

import cz.dat.gaben.api.interfaces.IInputManager;
import cz.trigon.ld37.screen.Playground;

public abstract class EntityInputListener extends Entity implements IInputManager.IInputListener {
    public EntityInputListener(Playground p) {
        super(p);
    }

    @Override
    public void onKeyDown(int key) {

    }

    @Override
    public void onKeyUp(int key) {

    }

    @Override
    public void onChar(char typedChar) {

    }

    @Override
    public void onMousePositionChange(float x, float y) {

    }

    @Override
    public void onMouseButtonDown(int button) {

    }

    @Override
    public void onMouseButtonUp(int button) {

    }

    @Override
    public void onMouseScroll(float x, float y) {

    }
}
