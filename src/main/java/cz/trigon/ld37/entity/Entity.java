package cz.trigon.ld37.entity;

import cz.dat.gaben.api.interfaces.IRenderer;
import cz.trigon.ld37.LDGame;
import cz.trigon.ld37.screen.Playground;
import cz.trigon.ld37.world.IBoundingBox;

public abstract class Entity {

    protected Playground playground;
    protected LDGame game;
    protected IRenderer render;

    protected int priority;
    protected int warpForce;

    public Entity(Playground p) {
        this.playground = p;
        this.game = (LDGame) p.getGame();
        this.render = this.game.getApi().getRenderer();
    }

    public int getWarpForce() {
        return this.warpForce;
    }

    public abstract void setDead();

    public int getTickPriority() {
        return this.priority;
    }

    public abstract void tick();

    public abstract void renderGame(float ptt);

    public abstract void renderUI(float ptt);

    public abstract IBoundingBox getBoundingBox();

    public abstract boolean isDead();
}
