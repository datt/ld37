package cz.trigon.ld37.entity;

import cz.dat.gaben.api.interfaces.IRenderer;
import cz.dat.gaben.util.Color;
import cz.trigon.ld37.screen.Playground;
import cz.trigon.ld37.world.CircleBB;
import cz.trigon.ld37.world.IBoundingBox;

public class Bullet extends Entity {

    private CircleBB bb, lastBB;
    private int colour = Color.color(1f, 1f, 0f, 0.9f);
    private float xV, yV, velF;
    private boolean dead = false;

    public Bullet(Playground p, float x, float y, float xVel, float yVel) {
        super(p);

        this.bb = new CircleBB(6, x, y);
        this.lastBB = bb;
        this.xV = xVel;
        this.yV = yVel;
        this.velF = 1f;
        this.warpForce = -3;
        this.priority = 1;
    }

    public void setVelFactor(float f) {
        this.velF = f;
    }

    public float getVelFactor() {
        return this.velF;
    }

    @Override
    public void setDead() {
        this.dead = true;
    }

    @Override
    public void tick() {
        this.lastBB = this.bb;
        this.bb = new CircleBB(this.bb.radius, this.bb.center.x() + this.xV * this.velF,
                this.bb.center.y() + this.yV * this.velF);
    }

    @Override
    public void renderGame(float ptt) {
        CircleBB renderBB = this.lastBB.mix(this.bb, ptt);
        this.render.enableTexture(false);
        this.render.color(this.colour);
        this.render.shapeMode(IRenderer.ShapeMode.FILLED);
        this.render.drawCircle(renderBB.center, renderBB.radius);
    }

    @Override
    public void renderUI(float ptt) {

    }

    @Override
    public IBoundingBox getBoundingBox() {
        return this.bb;
    }

    @Override
    public boolean isDead() {
        return this.dead;
    }
}
