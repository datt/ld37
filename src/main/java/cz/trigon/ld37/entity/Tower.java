package cz.trigon.ld37.entity;

import cz.dat.gaben.api.interfaces.IFont;
import cz.dat.gaben.api.interfaces.IFontRenderer;
import cz.dat.gaben.api.interfaces.IRenderer;
import cz.dat.gaben.util.Anchor;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.Vector2;
import cz.trigon.ld37.screen.Playground;
import cz.trigon.ld37.world.CircleBB;
import cz.trigon.ld37.world.IBoundingBox;

public abstract class Tower extends EntityInputListener {

    protected float rotation, lastRotation;
    protected CircleBB bb, lastBB, actionBB;

    protected float healthLeft;
    protected float actionRadius;
    protected float opsPerTick;
    protected float dmgMultiplier;
    protected int level, maxLevel;
    protected boolean showingUI = false;

    private IFontRenderer fr;
    private IFont font;

    public Tower(Playground p, float r, float x, float y) {
        super(p);
        this.fr = this.game.getApi().getFontRenderer();
        this.font = this.game.getApi().getFont().getFont("menuFont");

        this.priority = 2;
        this.bb = new CircleBB(r, x, y);
        this.lastBB = this.bb;
        this.healthLeft = 1f;
        this.level = 1;
    }

    public void hurt(float l) {
        this.healthLeft -= l;
        if (this.healthLeft < 0)
            this.healthLeft = 0;
    }

    public int getLevel() {
        return this.level;
    }

    public boolean levelUp() {
        if (this.level != this.maxLevel) {
            if (this.playground.getPoints() >= this.getNextLevelCost()) {
                this.opsPerTick = this.getNextOpsPerTick();
                this.dmgMultiplier = this.getNextDamageMult();
                this.setActionRadius(this.getNextActionRadius());
                this.playground.removePoints(this.getNextLevelCost());

                this.level++;
                if(this.healthLeft < 0.5f)
                    this.healthLeft = 0.5f;
                return true;
            }
        }

        return false;
    }

    public float getHealthLeft() {
        return this.healthLeft;
    }

    public CircleBB getActionBB() {
        return this.actionBB;
    }

    public void setActionRadius(float r) {
        this.actionRadius = r;
        this.actionBB = new CircleBB(this.actionRadius, this.bb.center);
    }

    public float getOpsPerTick() {
        return this.opsPerTick;
    }

    public float getDamageMult() {
        return this.dmgMultiplier;
    }

    public abstract int getNextLevelCost();

    public abstract float getNextOpsPerTick();

    public abstract float getNextActionRadius();

    public abstract float getNextDamageMult();

    @Override
    public void setDead() {
        this.healthLeft = 0;
    }

    @Override
    public void tick() {

        if (this.playground.getLevel().hasStarted()) {
            this.showingUI = false;
        } else {
            Vector2 pos = this.playground.unprojectCoord(this.game.getApi().getInput().getMousePosition());

            this.showingUI = this.bb.contains(pos.x(), pos.y());
        }

        this.lastBB = this.bb;
        this.lastRotation = this.rotation;
    }

    @Override
    public void renderGame(float ptt) {
        CircleBB render = this.lastBB.mix(this.bb, ptt);
        float rot = this.lastRotation + (this.rotation - this.lastRotation) * ptt;
        this.renderGame(render, rot, ptt);
    }

    @Override
    public void renderUI(float ptt) {
        if (this.showingUI) {
            this.fr.setFont(this.font);
            this.fr.setAnchor(Anchor.TOP_LEFT);
            this.fr.setSize(20);

            float boxW = 260, boxH = this.fr.getMaxHeight() * 6 + 20 + 30;
            float x, y;

            boolean facingLeft, facingTop;
            facingLeft = (this.bb.center.x() + this.bb.radius + boxW) > this.game.getWidth();
            facingTop = (this.bb.center.y() + this.bb.radius + boxH) > this.game.getHeight();
            x = 15 + (facingLeft ? (this.bb.center.x() + this.bb.radius - boxW) : (this.bb.center.x() + this.bb.radius));
            y = 15 + (facingTop ? (this.bb.center.y() + this.bb.radius - boxH) : (this.bb.center.y() + this.bb.radius));

            this.render.enableTexture(false);
            this.render.color(Color.color(0.2f, 0.2f, 0.2f, 0.5f));
            this.render.shapeMode(IRenderer.ShapeMode.FILLED);
            this.render.drawRect(x - 15, y - 15, x + boxW, y + boxH);
            this.render.color(Color.WHITE);
            this.render.shapeMode(IRenderer.ShapeMode.OUTLINE);
            this.render.drawRect(x - 15, y - 15, x + boxW, y + boxH);

            if (this.level != this.maxLevel) {
                this.fr.drawString("Level: " + this.level, x, y);
                y += this.fr.getMaxHeight();

                this.fr.drawString(String.format("Ops/sec: %.1f (+%.1f)", (this.opsPerTick * 20f),
                        (this.getNextOpsPerTick() * 20f)), x, y);
                y += this.fr.getMaxHeight();

                this.fr.drawString(String.format("Radius: %.1f (+%.1f)", this.actionRadius,
                        this.getNextActionRadius()), x, y);
                y += this.fr.getMaxHeight();

                this.fr.drawString(String.format("Shields: %d%% (+%d%%)", (int) (100f - this.dmgMultiplier * 100f),
                        (int) (100f - this.getNextDamageMult() * 100)), x, y);
                y += this.fr.getMaxHeight();

                this.fr.drawString(String.format("Health: %d%%", (int) (this.healthLeft * 100f)), x, y);
                y += this.fr.getMaxHeight() + 20;

                if (this.getNextLevelCost() > this.playground.getPoints()) {
                    this.render.color(Color.RED);
                } else {
                    this.render.color(Color.GREEN);
                }

                this.fr.drawString("Cost: " + this.getNextLevelCost(), x, y);
                y += this.fr.getMaxHeight();

                this.render.color(Color.WHITE);
                this.fr.setSize(16);
                this.fr.drawString("Click to upgrade", x, y);
            } else {
                this.fr.drawString("Level: " + this.level + " (max)", x, y);
                y += this.fr.getMaxHeight();

                this.fr.drawString(String.format("Ops/sec: %.1f", (this.opsPerTick * 20f)), x, y);
                y += this.fr.getMaxHeight();

                this.fr.drawString(String.format("Radius: %.1f", this.actionRadius), x, y);
                y += this.fr.getMaxHeight();

                this.fr.drawString(String.format("Shields: %d%%", (int) (100f - this.dmgMultiplier * 100f)), x, y);
                y += this.fr.getMaxHeight();

                this.fr.drawString(String.format("Health: %d%%", (int) (this.healthLeft * 100f)), x, y);
                y += this.fr.getMaxHeight() + 20;
            }
        }
    }

    protected abstract void renderGame(CircleBB renderBB, float rotation, float ptt);

    @Override
    public IBoundingBox getBoundingBox() {
        return this.bb;
    }

    @Override
    public boolean isDead() {
        return this.healthLeft <= 0;
    }

    @Override
    public void onMouseButtonDown(int button) {
        if (!this.playground.getLevel().hasStarted()) {
            Vector2 pos = this.playground.unprojectCoord(this.game.getApi().getInput().getMousePosition());

            if (this.bb.contains(pos.x(), pos.y())) {
                if (button == 0) {
                    if (this.levelUp()) {
                        this.game.getApi().getSound().playSound("menuMove");
                    } else {
                        this.game.getApi().getSound().playSound("menuDenied");
                    }
                }
            }
        }
    }
}
