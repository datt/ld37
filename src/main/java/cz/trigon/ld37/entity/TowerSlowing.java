package cz.trigon.ld37.entity;

import cz.dat.gaben.api.interfaces.IRenderer;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.Vector2;
import cz.trigon.ld37.screen.Playground;
import cz.trigon.ld37.world.CircleBB;

import java.util.ArrayList;
import java.util.List;

public class TowerSlowing extends Tower {

    private float cR = 0.5f, cG = 0.4f, cB = 0.3f;

    public TowerSlowing(Playground p, float r, float x, float y) {
        super(p, r, x, y);

        this.maxLevel = 10;
        this.opsPerTick = 1 / 10f;
        this.warpForce = -5;
        this.dmgMultiplier = 1f;
        this.setActionRadius(r + 25);
    }

    @Override
    public boolean levelUp() {
        if (super.levelUp()) {
            this.cR *= 1.1f;
            this.cG *= 1.1f;
            this.cB *= 1.05f;
            this.bb.radius *= 1.069f;
            this.warpForce *= 1.25;
            return true;
        }

        return false;
    }

    @Override
    public int getNextLevelCost() {
        return this.level * this.level * (1000 / (int) Math.sqrt(this.level));
    }

    @Override
    public float getNextOpsPerTick() {
        return this.opsPerTick + (2f / this.level);
    }

    @Override
    public float getNextActionRadius() {
        return this.actionRadius * 1.15f;
    }

    @Override
    public float getNextDamageMult() {
        return this.dmgMultiplier * 0.9f;
    }

    private float ops;
    private List<Entity> slowingDown = new ArrayList<>();

    @Override
    public void tick() {
        super.tick();
        this.rotation += 5;

        this.ops += this.opsPerTick;
        this.slowRandomEnemy();

    }

    private void slowRandomEnemy() {
        this.playground.getEntities(Bullet.class).forEach(e -> {
            if (this.actionBB.collidesWith(e.getBoundingBox())) {
                if (this.ops > 1f && !this.slowingDown.contains(e)) {
                    this.slowingDown.add(e);
                    e.setVelFactor(e.getVelFactor() * 0.6f);
                    this.ops--;
                }
            } else {
                if (this.slowingDown.contains(e)) {
                    this.slowingDown.remove(e);
                    e.setVelFactor(e.getVelFactor() * (1f / 0.6f));
                }
            }
        });
    }

    @Override
    protected void renderGame(CircleBB renderBB, float rotation, float ptt) {
        this.render.enableTexture(false);
        this.render.shapeMode(IRenderer.ShapeMode.OUTLINE);
        this.render.color(Color.color(this.cR + (1f - this.cR) * (1f - this.healthLeft),
                this.cG * this.healthLeft, this.cB * this.healthLeft, 1f));

        this.render.setMatrix(this.render.identityMatrix().rotateFrom(renderBB.center.x(),
                renderBB.center.y(), rotation));
        this.render.drawSegCircle(renderBB.center, renderBB.radius, 7 + this.level);
        this.render.drawSegCircle(renderBB.center, renderBB.radius * 0.8f, 4 + this.level);
        this.render.setMatrix(this.render.identityMatrix());

        for (Entity e : this.slowingDown) {
            if (!e.isDead())
                this.render.drawLine(renderBB.center, e.getBoundingBox().getOrigin());
        }

        Vector2 m = this.playground.unprojectCoord(this.game.getApi().getInput().getMousePosition());
        if (renderBB.contains(m.x(), m.y())) {
            this.render.color(1f, 1f, 1f, 0.1f);
            this.render.drawCircle(this.actionBB.center, this.actionBB.radius);
        }
    }
}
